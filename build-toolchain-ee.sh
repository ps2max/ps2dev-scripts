#!/bin/bash

./checkenv.sh || { exit 1; }

# Download gcc prerequisites
cd toolchain/ee/gcc || { exit 1; }
contrib/download_prerequisites || { exit 1; }
cd ../../.. || { exit 1; }

# EE toolchain
./build-binutils.sh      ee ee mips64r5900el-ps2-elf || { exit 1; }
./build-gcc-stage1-ee.sh ee ee mips64r5900el-ps2-elf || { exit 1; }
./build-newlib.sh        ee ee mips64r5900el-ps2-elf || { exit 1; }
./build-newlib-nano.sh   ee ee mips64r5900el-ps2-elf || { exit 1; }
./build-pthread-ee.sh                                || { exit 1; }
./build-gcc-stage2-ee.sh ee ee mips64r5900el-ps2-elf || { exit 1; }
