#!/bin/bash

./checkenv.sh || { exit 1; }

SOURCE_PATH=$1
TARGET_ALIAS=$2
if [ -z "$3" ]
then
  TARGET=$TARGET_ALIAS
else
  TARGET=$3
fi

## Determine the maximum number of processes that Make can work with.
PROC_NR=$(getconf _NPROCESSORS_ONLN)

## Create and enter the toolchain/build directory
mkdir -p toolchain/build || { exit 1; }
cd       toolchain/build || { exit 1; }

## Create and enter the build directory.
rm -rf newlib-nano-$TARGET_ALIAS || { exit 1; }
mkdir  newlib-nano-$TARGET_ALIAS || { exit 1; }
cd     newlib-nano-$TARGET_ALIAS || { exit 1; }

## Configure the build.
CFLAGS_FOR_TARGET="-G0 -O2" ../../$SOURCE_PATH/newlib/configure \
  --prefix="$PSMAX_ROOT/$TARGET_ALIAS/newlib-nano" \
  --target="$TARGET" \
  --with-sysroot="$PSMAX_ROOT/$TARGET_ALIAS/$TARGET" \
  --disable-newlib-supplied-syscalls \
  --enable-newlib-reent-small \
  --disable-newlib-fvwrite-in-streamio \
  --disable-newlib-fseek-optimization \
  --disable-newlib-wide-orient \
  --enable-newlib-nano-malloc \
  --disable-newlib-unbuf-stream-opt \
  --enable-lite-exit \
  --enable-newlib-global-atexit \
  --enable-newlib-nano-formatted-io \
  --enable-newlib-retargetable-locking \
  --enable-newlib-multithread \
  --disable-nls || { exit 1; }

## Compile and install.
make -j $PROC_NR clean         || { exit 1; }
#mkdir -p $TARGET/newlib/libc/sys/ps2/.deps/ || { exit 1; }
make -j $PROC_NR all           || { exit 1; }
make -j $PROC_NR install-strip || { exit 1; }

## Copy & rename manually libc, libg and libm to libc-nano, libg-nano and libm-nano
ln -s "$PSMAX_ROOT/$TARGET_ALIAS/newlib-nano/$TARGET/lib/libc.a" "$PSMAX_ROOT/$TARGET_ALIAS/$TARGET/lib/libc_nano.a"
ln -s "$PSMAX_ROOT/$TARGET_ALIAS/newlib-nano/$TARGET/lib/libg.a" "$PSMAX_ROOT/$TARGET_ALIAS/$TARGET/lib/libg_nano.a"
ln -s "$PSMAX_ROOT/$TARGET_ALIAS/newlib-nano/$TARGET/lib/libm.a" "$PSMAX_ROOT/$TARGET_ALIAS/$TARGET/lib/libm_nano.a"

## Exit the build directory
cd .. || { exit 1; }

## Exit the toolchain/build directory
cd ../.. || { exit 1; }
