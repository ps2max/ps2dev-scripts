#!/bin/bash

./checkenv.sh || { exit 1; }

## NOTE: Building a new toolchain will also wipe the $PSMAX_ROOT folder
rm -rf $PSMAX_ROOT/*
rm -rf toolchain/build

./build-toolchain-iop.sh     || { exit 1; }
#./build-toolchain-iop-ppc.sh || { exit 1; }
./build-toolchain-ee.sh || { exit 1; }
./build-toolchain-dvp.sh || { exit 1; }
