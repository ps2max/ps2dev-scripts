#!/bin/bash

if [ -z "$PS2DEV" ]; then
  echo "ERROR: PS2DEV environment not set!!!"
  echo "execute \"source envsetup.sh\" first"
  exit 1
fi

if [ "$PS2DEV" != "$PSMAX_ROOT" ]; then
  echo "ERROR: Wrong environment!!!"
  echo "    PS2DEV     = $PS2DEV"
  echo "    PSMAX_ROOT = $PSMAX_ROOT"
  exit 1
fi
