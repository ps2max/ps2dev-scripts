#!/bin/bash

./checkenv.sh || { exit 1; }

SOURCE_PATH=$1
TARGET_ALIAS=$2
if [ -z "$3" ]
then
  TARGET=$TARGET_ALIAS
else
  TARGET=$3
fi

OSVER=$(uname)
## Apple needs to pretend to be linux
if [ ${OSVER:0:6} == Darwin ]; then
	TARG_XTRA_OPTS="--build=i386-linux-gnu --host=i386-linux-gnu"
else
	TARG_XTRA_OPTS=""
fi

## Determine the maximum number of processes that Make can work with.
PROC_NR=$(getconf _NPROCESSORS_ONLN)

## Create and enter the toolchain/build directory
mkdir -p toolchain/build || { exit 1; }
cd       toolchain/build || { exit 1; }

## Create and enter the build directory.
rm -rf gcc-$TARGET_ALIAS-stage1 || { exit 1; }
mkdir  gcc-$TARGET_ALIAS-stage1 || { exit 1; }
cd     gcc-$TARGET_ALIAS-stage1 || { exit 1; }

## Configure the build.
../../$SOURCE_PATH/gcc/configure \
  --prefix="$PSMAX_ROOT/$TARGET_ALIAS" \
  --target="$TARGET" \
  --enable-languages="c" \
  --with-float=hard \
  --without-headers \
  --without-newlib \
  --disable-libgcc \
  --disable-shared \
  --disable-threads \
  --disable-multilib \
  --disable-libatomic \
  --disable-nls \
  --disable-tls \
  --disable-libssp \
  --disable-libgomp \
  --disable-libmudflap \
  --disable-libquadmath \
  $TARG_XTRA_OPTS || { exit 1; }

## Compile and install.
make -j $PROC_NR clean         || { exit 1; }
make -j $PROC_NR all           || { exit 1; }
make -j $PROC_NR install-strip || { exit 1; }

## Exit the build directory
cd .. || { exit 1; }

## Exit the toolchain/build directory
cd ../.. || { exit 1; }
