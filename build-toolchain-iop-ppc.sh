#!/bin/bash

./checkenv.sh || { exit 1; }

# Download gcc prerequisites
cd toolchain/generic/gcc || { exit 1; }
contrib/download_prerequisites || { exit 1; }
cd ../../.. || { exit 1; }

# IOP-PPC toolchain
./build-binutils.sh   generic iop-ppc powerpc-none-eabi || { exit 1; }
./build-gcc-stage1.sh generic iop-ppc powerpc-none-eabi || { exit 1; }
