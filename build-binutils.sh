#!/bin/bash

./checkenv.sh || { exit 1; }

SOURCE_PATH=$1
TARGET_ALIAS=$2
if [ -z "$3" ]
then
  TARGET=$TARGET_ALIAS
else
  TARGET=$3
fi
TARG_XTRA_OPTS=""

## Determine the maximum number of processes that Make can work with.
PROC_NR=$(getconf _NPROCESSORS_ONLN)

## Create and enter the toolchain/build directory
mkdir -p toolchain/build || { exit 1; }
cd       toolchain/build || { exit 1; }

## Create and enter the build directory.
rm -rf binutils-$TARGET_ALIAS || { exit 1; }
mkdir  binutils-$TARGET_ALIAS || { exit 1; }
cd     binutils-$TARGET_ALIAS || { exit 1; }

## Configure the build.
../../$SOURCE_PATH/binutils/configure \
  --prefix="$PSMAX_ROOT/$TARGET_ALIAS" \
  --target="$TARGET" \
  --with-sysroot="$PSMAX_ROOT/$TARGET_ALIAS/$TARGET" \
  --disable-separate-code \
  --disable-sim \
  --disable-nls \
  $TARG_XTRA_OPTS || { exit 1; }

## Compile and install.
make -j $PROC_NR clean         || { exit 1; }
make -j $PROC_NR all           || { exit 1; }
make -j $PROC_NR install       || { exit 1; }
#make -j $PROC_NR install-strip || { exit 1; }

## Exit the build directory.
cd .. || { exit 1; }

## Exit the toolchain/build directory
cd ../.. || { exit 1; }
