#!/bin/bash

./checkenv.sh || { exit 1; }

# DVP toolchain
./build-binutils.sh dvp dvp || { exit 1; }
cd toolchain/dvp/openvcl
make clean all install || { exit 1; }
cd ../vclpp
make clean all install || { exit 1; }
cd ../../..
