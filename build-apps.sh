#!/bin/bash

./checkenv.sh || { exit 1; }

cd apps

#cd FMCBInstaller     && make clean all     && cd .. || { exit 1; }
#cd HDDChecker        && make clean all     && cd .. || { exit 1; }
#cd HDLGameInstaller  && make clean all     && cd .. || { exit 1; }
cd open-ps2-loader   && make clean release && cd .. || { exit 1; }
#cd pademu_playground && make clean all     && cd .. || { exit 1; }
#cd PS2ESDL           && make clean all     && cd .. || { exit 1; }
#cd PS2Ident          && make clean all     && cd .. || { exit 1; }
cd ps2link           && make clean all     && cd .. || { exit 1; }
# Kernelloader depends on ps2link.irx
#cp ps2link/iop/ps2link.irx $PS2SDK/iop/irx/         || { exit 1; }
#cd kernelloader      && make clean all     && cd .. || { exit 1; }
cd neutrino          && make clean all     && cd .. || { exit 1; }
#cd speed-testing     && make clean all     && cd .. || { exit 1; }
cd uLaunchELF        && make clean all     && cd .. || { exit 1; }

cd ..
