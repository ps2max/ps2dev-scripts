#!/bin/bash

./checkenv.sh || { exit 1; }

cd tools

cd ps2client
make clean all install || { exit 1; }
cd ..

cd ps2-packer
make clean all install || { exit 1; }
cd ..

#cd romimg
#mkdir -p build || { exit 1; }
#cd build
#cmake .. -DCMAKE_BUILD_TYPE=Release
#make clean all || { exit 1; }
#cp ROMIMG $PS2DEV/bin || { exit 1; }
#cd ..
#rm -rf build
#cd ..

cd ..
