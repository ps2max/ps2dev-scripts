#!/bin/bash

## Determine the maximum number of processes that Make can work with.
## Also make preparations for different toolchains
PROC_NR=$(getconf _NPROCESSORS_ONLN)
CFLAGS=""
XTRA_OPTS=""
MAKECMD=make
OSVER=$(uname)
if [ ${OSVER:0:5} == MINGW ]; then
  XTRA_OPTS=(. -G"MinGW Makefiles")
  MAKECMD=${OSVER:0:7}-make
else
  XTRA_OPTS=(. -G"Unix Makefiles")
fi

CMAKE_OPTIONS=(-Wno-dev "-DCMAKE_TOOLCHAIN_FILE=$PS2DEV/share/ps2dev.cmake" "-DCMAKE_INSTALL_PREFIX=$PS2SDK/ports" -DBUILD_SHARED_LIBS=OFF -DCMAKE_BUILD_TYPE=RelWithDebInfo "-DCMAKE_PREFIX_PATH=$PS2SDK/ports")
#CMAKE_OPTIONS=("${CMAKE_OPTIONS[@]}" -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON)

function build {
    START_DIR="${PWD}"
    rm -rf build/$1
    mkdir -p build/$1
    cd build/$1
    CFLAGS="$CFLAGS" cmake "${CMAKE_OPTIONS[@]}" "$@" "${XTRA_OPTS[@]}" ../../$1 || { exit 1; }
    "${MAKECMD}" --quiet -j "$PROC_NR" clean all install || { exit 1; }
    cd "${START_DIR}"
}

cd libs

##
## Build ps2sdk
##
rm -rf $PS2SDK
cd ps2sdk
make clean all install || { exit 1; }
cd ..

## gcc needs to include both libc and libkernel from ps2sdk to be able to build executables.
## NOTE: There are TWO libc libraries, gcc needs to include them both.
ln -sf "$PS2SDK/ee/lib/libcglue.a"       "$PS2DEV/ee/mips64r5900el-ps2-elf/lib/libcglue.a"       || { exit 1; }
ln -sf "$PS2SDK/ee/lib/libpthreadglue.a" "$PS2DEV/ee/mips64r5900el-ps2-elf/lib/libpthreadglue.a" || { exit 1; }
ln -sf "$PS2SDK/ee/lib/libkernel.a"      "$PS2DEV/ee/mips64r5900el-ps2-elf/lib/libkernel.a"      || { exit 1; }
ln -sf "$PS2SDK/ee/lib/libcdvd.a"        "$PS2DEV/ee/mips64r5900el-ps2-elf/lib/libcdvd.a"        || { exit 1; }

##
## Build cmake libraries part 1 (needed by gsKit)
##
PROC_NR=1 build zlib "-DUNIX:BOOL=ON" # Forcing to compile with -j1 because there is a race condition in zlib
build xz -DTUKLIB_CPUCORES_FOUND=ON -DTUKLIB_PHYSMEM_FOUND=ON -DHAVE_GETOPT_LONG=OFF -DBUILD_TESTING=OFF
build libpng -DPNG_SHARED=OFF -DPNG_STATIC=ON
build libjpeg-turbo -DENABLE_SHARED=FALSE -DWITH_SIMD=0
CFLAGS="-Dlfind=bsearch" build libtiff -Dtiff-tools=OFF -Dtiff-tests=OFF

##
## Build ps2sdk-ports libraries part 1 (needed by gsKit)
##
cd ps2sdk-ports
cd libjpeg_ps2_addons
make clean all install || { exit 1; }
cd ..
cd ..

##
## Build gsKit with all options enabled
##
build gsKit

##
## Build ps2-drivers (needed by SDL)
##
#cd ps2-drivers
#make clean all install || { exit 1; }
#cd ..

##
## Build cmake libraries part 2 (needed by SDL, and many others)
##
#CFLAGS="-DWOLFSSL_GETRANDOM -DNO_WRITEV" build wolfssl -DBUILD_SHARED_LIBS=OFF -DWOLFSSL_CRYPT_TESTS=OFF -DWOLFSSL_EXAMPLES=OFF -DWOLFSSL_OPENSSLEXTRA=ON -DWARNING_C_FLAGS=-w
#CFLAGS="-DSIZEOF_LONG=4 -DSIZEOF_LONG_LONG=8 -DNO_WRITEV" build curl -DBUILD_SHARED_LIBS=OFF -DENABLE_THREADED_RESOLVER=OFF -DCURL_USE_OPENSSL=OFF -DCURL_USE_WOLFSSL=ON -DCURL_DISABLE_SOCKETPAIR=ON -DHAVE_BASENAME=NO -DHAVE_ATOMIC=NO
build freetype2
build libyaml

##
## Sound
##
#build libmodplug
#build libxmp "-DBUILD_SHARED=OFF"
#build mikmod/libmikmod "-DENABLE_SHARED=0"
build ogg
#build opus
#build opusfile "-DOP_DISABLE_HTTP=ON -DOP_DISABLE_DOCS=ON -DOP_DISABLE_EXAMPLES=ON"
build vorbis

##
## Build SDL
##
#build SDL
#build SDL-mixer "-DCMAKE_POSITION_INDEPENDENT_CODE=OFF -DDSDL2MIXER_DEPS_SHARED=OFF -DSDL2MIXER_OPUS=OFF -DSDL2MIXER_MIDI=OFF -DSDL2MIXER_FLAC=OFF -DSDL2MIXER_MOD=ON -DSDL2MIXER_SAMPLES=OFF"
#build SDL-image "-DCMAKE_POSITION_INDEPENDENT_CODE=OFF -DBUILD_SHARED_LIBS=OFF"
#build SDL-ttf "-DBUILD_SHARED_LIBS=OFF -DCMAKE_POSITION_INDEPENDENT_CODE=OFF -DSDL2TTF_SAMPLES=OFF"

##
## Build others
##
cd isjpcm     && make clean all install && cd ..    || { exit 1; }
cd ps2stuff   && make clean all install && cd ..    || { exit 1; }
cd ps2gl      && make clean all install && cd ..    || { exit 1; }
cd ps2gl/glut && make clean all install && cd ../.. || { exit 1; }

cd ..
