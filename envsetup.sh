#!/bin/bash

if [ -z "$PSMAX_ROOT" ]; then
    # if no root defined, then set local PSMAX_ROOT
    # to enable multiple development environments
    export PSMAX_ROOT=$PWD/install
fi

if [ -n "$PS2DEV" ]; then
    if [ "$PS2DEV" == "$PSMAX_ROOT" ]; then
        echo "PS2DEV environment already set to $PS2DEV"
        return 0
    else
        echo "ERROR: PS2DEV environment set to $PS2DEV"
        echo "                   -> instead of $PSMAX_ROOT"
        return 1
    fi
fi

mkdir -p $PSMAX_ROOT

# Create needed vars PS2
export PS2DEV=$PSMAX_ROOT
export PS2SDK=$PS2DEV/ps2sdk
export GSKIT=$PS2DEV/gsKit
export PS2SDKSRC=$PWD/libs/ps2sdk
export GSKITSRC=$PWD/libs/gsKit

# Add binaries to PATH
export PATH=$PATH:$PS2DEV/bin
export PATH=$PATH:$PS2DEV/ee/bin
export PATH=$PATH:$PS2DEV/iop/bin
export PATH=$PATH:$PS2DEV/iop-ppc/bin
export PATH=$PATH:$PS2DEV/dvp/bin
export PATH=$PATH:$PS2SDK/bin

echo "PS2DEV environment set to $PS2DEV"

return 0
