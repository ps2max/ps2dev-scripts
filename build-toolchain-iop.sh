#!/bin/bash

./checkenv.sh || { exit 1; }

# Download gcc prerequisites
cd toolchain/iop/gcc || { exit 1; }
contrib/download_prerequisites || { exit 1; }
cd ../../.. || { exit 1; }

# IOP toolchain - custom
./build-binutils.sh       iop iop mipsel-ps2-irx || { exit 1; }
./build-gcc-stage1-iop.sh iop iop mipsel-ps2-irx || { exit 1; }

# Download gcc prerequisites
cd toolchain/generic/gcc || { exit 1; }
contrib/download_prerequisites || { exit 1; }
cd ../../.. || { exit 1; }

# IOP toolchain - generic
./build-binutils.sh       generic iop mipsel-none-elf || { exit 1; }
./build-gcc-stage1-iop.sh generic iop mipsel-none-elf || { exit 1; }

cd toolchain/iop/iopmod/
make clean tool || { exit 1; }
cp tool/iopmod-info $PS2DEV/iop/bin
cp tool/iopmod-link $PS2DEV/iop/bin
cp tool/iopmod-symc $PS2DEV/iop/bin
cd ../../..
