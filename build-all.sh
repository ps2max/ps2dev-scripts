#!/bin/bash

./checkenv.sh || { exit 1; }

./build-toolchain.sh || { exit 1; }
./build-libs.sh      || { exit 1; }
./build-tools.sh     || { exit 1; }
./build-apps.sh      || { exit 1; }
./repo-cleanup.sh    || { exit 1; }
