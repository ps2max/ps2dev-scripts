#!/bin/bash

./checkenv.sh || { exit 1; }

## Determine the maximum number of processes that Make can work with.
PROC_NR=$(getconf _NPROCESSORS_ONLN)

cd toolchain/ee/pthread-embedded/platform/ps2 || { exit 1; }
make -j $PROC_NR clean   || { exit 1; }
make -j $PROC_NR all     || { exit 1; }
make -j $PROC_NR install || { exit 1; }
cd ../../../../.. || { exit 1; }
